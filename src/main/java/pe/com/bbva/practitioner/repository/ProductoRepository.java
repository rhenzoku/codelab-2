package pe.com.bbva.practitioner.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import pe.com.bbva.practitioner.models.ProductoModel;  
@Repository public interface ProductoRepository extends MongoRepository<ProductoModel, String> { } 